grid_size = 10
rows = range (0,grid_size)
columns = range (0,grid_size)
ColRange = "012345678910"
RowRange = "abcdefghij"
shipnames = ("destroyer", "submarine", "cruiser", "carrier", "aircraft carrier")


class player():
    
    def __init__(self):

        self.grid = [ ]

        for row in rows:
    
            self.grid.append([])

            for column in columns:
                self.grid[row].append(["-", 0])

# print grid

    def print_grid(self, map_val):  # map 0 is bombs; map 1 is ships

        for column in columns:
  
            print (" ", column +1, " ", end="")
        print()
        print()


        for row in rows:
            print (chr (65+row), end="")
            for column in columns:
                print (" ", self.grid[row][column][map_val], " ",end="")
            print ("\n")

    
    def ConvertToNum(self,num_str):
    
        number  = ord (num_str)-97 # convert character to number; note a = ascii 97
        return (number)
    
   
    
    def mark_grid(self,row,column,status):

        if status == "hit":

            self.grid[row][column][0] = "X"


        if status == "miss":

            self.grid[row][column][0] = "O"
    


    def HitOrMiss(self,row, column):

        hit = False # define hit (default = false)
        check =self.grid[row][column][1]
        
        if check != 0:  # 0 = empty ; 2-5 = ship class
        
            hit = True
        
        return (hit,check)


    def AddShips(self,ships):

        for ship in range (2,ships):

            self.print_grid(1) # print grid showing ships
        
            orient = ""
            print ("adding ", shipnames[ship-2], " length ", ship, "units: ", end="")

            while (orient != "h") and (orient != "v"):

                orient=input("h for horizontal and v for vertical?: ")
            print ("orient = ", orient)

        # check whether ship is within grid

            legal_space = False # assume space is illegal until proven legal
        
            while not legal_space:

                new_ship = self.GetCoord() # ship is used by get_coord to check to se if the ship will fit
                row=new_ship[0]
                column=new_ship[1]
            
                if orient == "h":
                    if column + ship <= 10:
                        legal_space = True # ship fits in grid

#check to see if ship location would overwrite another ship
                    
                        check_grid = self.grid[row][column:column+ship]
                        print ("check_grid: ", check_grid)
                           
                        red_flag = False # reset red flag

                        for item in check_grid:
                            if item[1] != 0:
                                red_flag = True # ships would be on top of each other

                        if red_flag == True:
                            print (" requested ship location conflicts with existing ship")
                            self.print_grid (1) # print ship layout
                            legal_space = False # 
                
                if orient == "v":
                    if row + ship <= 10:
                        legal_space = True
                        red_flag = False
                    

#check to see if ship location would overwrite another ship

                    for count in range (0,ship):
                    
                        check_row = self.grid[row+count]
                        check_cell = check_row [column]

                        if check_cell[1] != 0:
                            red_flag = True # ships would be on top of each other

                            if red_flag == True:
                                print (" requested ship location conflicts with existing ship")
                                self.print_grid (1) # print ship layout
                                legal_space = False # 


#  now that we know ship location is legal, put ship on grid
    
        
            if orient == "h": # add horizontal units of ship
                        
                for ShipUnit in range (0,ship):      
                     self.grid[row][column+ShipUnit][1]= ship # Ship unit number is used for size of ship and type of ship 

        
            if orient == "v": # add vertical units of ship
                for ShipUnit in range (0,ship):
                     self.grid[row+ShipUnit][column][1]= ship  # Ship unit number is used for size of ship and type of ship

        print ("added ", shipnames[ship-2])


    def GetCoord (self):
        xStr = ""
        yStr = ""
        row = -1
        column = -1

        while xStr == "":

            xStr=input("row :")
            try:
           
                row = ord(xStr)-97 #convert a to 1, b to 2, etc
                if row in range (0,11): # check to see if row is legal

                    continue
                else:
                    xStr =""
                    continue
            
            except:
                xStr =""
                continue
            


        while yStr == "":
            yStr = input ("column :")
            try:
                column = int (yStr)-1
                if column < 0 and column >10:
                    yStr=""
            except:
                yStr=""
                continue    
      
        return (row,column)



    
def clear_screen():
    print ("\n" * 60)

# main

row = 0
column = 0
bomb_map = 0
ship_map = 1
ships = 6 # lenght of ships minus 1 

ships_left= [4,4] #number of ships for player 1 and 2
hits_remaining = [[2,3,4,5],[2,3,4,5]]  # ship 0 is 2 units long, ship 1 is 3 units, etc.; ships for player 1 and 2


players=[]
players.append(player()) # add player 0 aka " player 1"
players.append(player()) # add player 1 aka " player 2"

clear_screen()

# each player adds ships

for player_now in range (0,2):
    print ("player No.", player_now+1,": supply coordinates for your ships")
    players[player_now].AddShips(ships)
    players[player_now].print_grid(1) # print grid showing ships    
    x = input ("hit return to continue")
    clear_screen()

x = input ("ready")

# ships are added.  Now let's blow stuff up

game_over = False

while game_over == False:
    
    for player_now in range (0,2):  # players to take turns

        if player_now ==0:
            player_other =1
        else:
            player_other =0
            

        print ("player ", player_other+1, "map ")        
        players[player_other].print_grid(0)
        
        print ("player No.", player_now+1,": supply missle coordinates")    
    
        check_coord = players[player_other].GetCoord()
        row = check_coord[0]
        column = check_coord[1]

        hit = players[player_other].HitOrMiss(row,column)
      
        if hit[0] == True:
            status = "hit"
            hits_remaining[player_other][hit[1]-2] -=1


            # hit [1] = ship type
            # ship type -2 = adjusts type in relationship to the list.  ie, type 2 = is the first ship in the list aka ship_hits_remaining[0]


            print ("hits remaining ", hits_remaining[player_other])
            players[player_other].mark_grid(row,column, status)

            if hits_remaining[player_other][hit[1]-2] == 0:
                print ("You sunk my ", shipnames [ hit[1]-2])

                ships_left[player_other] -=1
                print ("ships remaining:", ships_left[player_other])

                if ships_left[player_other] ==0:
                    print ("Player No. ", player_other + 1, "surrenders" * 1000)
                    x = "exit"
            
        else:
            status = "miss"
            players[player_other].mark_grid (row, column, status)

        print ("player ", player_other+1, " has ships left to sink: ", ships_left[player_other])

        print ("player ", player_other+1, "map ")
        players[player_other].print_grid(bomb_map)
        x=input ("press return to continue")
        
 
x = input (" hit return to leave")


    

    




    
    
    
