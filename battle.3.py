grid_size = 10
rows = range (0,grid_size)
columns = range (0,grid_size)
ColRange = "012345678910"
RowRange = "abcdefghij"
shipnames = ("destroyer", "submarine", "cruiser", "carrier", "aircraft carrier")

grid = [ ]

def init_grid ():
    
# init

    for row in rows:
    
        grid.append([])
        for column in columns:
            grid[row].append(["-", 0])

    return
# print grid

def print_grid(map):  # map 0 is bombs; map 1 is ships
    for column in columns:
  
        print (" ", column +1, " ", end="")
    print()
    print()


    for row in rows:
        print (chr (65+row), end="")
        for column in columns:
            print (" ", grid[row][column][map], " ",end="")
        print ("\n")

    return
    
   
    
def ConvertToNum(numstr):
    
    number  = ord (numstr)-97 # convert character to number; note a = ascii 97
    return (number)
    
   
    
def mark_grid(xCoord,yCoord,status):

    if status == "hit":

        grid[xCoord][yCoord][0] = "X"


    if status == "miss":

        grid[xCoord][yCoord][0] = "O"
    


def HitOrMiss(xCoord, yCoord):
    hit = False # define hit (default = false)
    check =grid[xCoord][yCoord][1]
    if check != 0:  # 0 = empty ; 2-5 = ship class
        
        hit = True
        
    return (hit,check)

def AddShips (ships):
    for ship in range (2,ships):

        print_grid(1) # print grid showing ships
        
        orient = ""
        print ("adding ", shipnames[ship-2], " length ", ship, "units: ", end="")

        while (orient != "h") and (orient != "v"):
            orient=input("h for horizontal and v for vertical?: ")
            print ("orient: ", orient)



        # check whether ship is within grid

        legal_space = False
        
        while not legal_space:

            new_ship = GetCoord() # ship is used by get_coord to check to se if the ship will fit
            row=new_ship[0]
            column=new_ship[1]
            
            if orient == "h":
                if column + ship <= 10:
                    legal_space = True

#check to see if ship location would overwrite another ship
                    
                    check_grid = grid[row][column:column+ship]
                    print ("check_grid: ", check_grid)
                           
                    red_flag = False # reset red flag

                    for item in check_grid:
                        if item[1] != 0:
                            red_flag = True # ships would be on top of each other
                    if red_flag == True:
                        print (" requested ship location conflicts with existing ship")
                        print_grid (1) # print ship layout
                        legal_space = False # 
                
            if orient == "v":
                if row + ship <= 10:
                    legal_space = True
                    red_flag = False
                    

#check to see if ship location would overwrite another ship

                    for count in range (0,ship):
                    
                        check_row = grid[row+count]
                        check_cell = check_row [column]


                        if check_cell[1] != 0:
                            red_flag = True # ships would be on top of each other

                    if red_flag == True:
                        print (" requested ship location conflicts with existing ship")
                        print_grid (1) # print ship layout
                        legal_space = False # 
# now that we know ship will be within grid, check to see if new ship would conflict with old ship

        

#  now that we know ship location is legal, put ship on grid

    
        
        if orient == "h": # add horizontal units of ship
            
            for ShipUnit in range (0,ship):
                
                 grid[row][column+ShipUnit][1]= ship # Ship unit number is used for size of ship and type of ship 

            print ("added ", shipnames[ship-2])

        if orient == "v": # add vertical units of ship

            for ShipUnit in range (0,ship):
                
                 grid[row+ShipUnit][column][1]= ship  # Ship unit number is used for size of ship and type of ship

            print ("added ", shipnames[ship-2])


def GetCoord ():
    xStr = ""
    yStr = ""
    row = -1
    column = -1

    while xStr == "":
        xStr=input("row :")
       
        
        try:
           
            row = ord(xStr)-97 #convert a to 1, b to 2, etc
            
         
            if row in range (0,11): # check to see if row is legal

                continue
            else:
                xStr =""
                continue
            
        except:
            xStr =""
            continue
            


    while yStr == "":
        yStr = input ("column :")
        try:
            column = int (yStr)-1
            if column < 0 and column >10:
                yStr=""
        except:
            yStr=""
            continue    
      
    return (row,column)

# main

row = 0
column = 0
bomb_map = 0
ship_map = 1
ships = 6 # lenght of ships minus 1 and number of ships minus 2

ships_hits_remaining = [2,3,4,5]
ships_left = ships - 2
init_grid()

AddShips(ships)
print_grid(1) # print grid showing ships

# ships are added.  Now let's blow stuff up

print ("    "*10000)
print_grid(0)

x = ""
while not x == "exit":
    
    check_coord = GetCoord()
    row = check_coord[0]
    column = check_coord[1]

    hit = HitOrMiss(row,column)
    

    
    if hit[0] == True:
        status = "hit"
        print ("ships_hits_remaining ", ships_hits_remaining)
        ships_hits_remaining[hit[1]-2] -=1
        print ("ships_hits_remaining ", ships_hits_remaining)
        mark_grid(row,column, status)

        if ships_hits_remaining[hit[1]-2] == 0:
            print ("You sunk my ", shipnames [ hit[1]-2])
            ships_left -=1
            if ships_left ==0:
                print ("I'm dead " * 1000)
                x = "exit"
            
    else:
        status = "miss"
        mark_grid (row, column, status)


    print_grid(bomb_map)
 
x = input (" hit return to leave")


    

    




    
    
    
